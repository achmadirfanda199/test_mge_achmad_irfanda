// ignore_for_file: prefer_const_constructors

import 'package:test_mge_achmad_irfanda/common_import_library.dart';
import 'package:test_mge_achmad_irfanda/page/apps_home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<Widget> futureCall() async {
    return Future.delayed(
      const  Duration(seconds: 3), 
      (
        ()=> AppsHomePage()
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return EasySplashScreen(
      logo: Image.network('https://cdn4.iconfinder.com/data/icons/logos-brands-5/24/flutter-512.png'),
      title: Text(
        "Title",
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
      futureNavigator: futureCall(),
      backgroundColor: Colors.grey.shade400,
      showLoader: true,
      loadingText: Text("Loading..."),
    );
  }
}
