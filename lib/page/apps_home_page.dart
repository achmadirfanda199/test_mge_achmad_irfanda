// apps home page

import 'package:test_mge_achmad_irfanda/common_import_library.dart';
import 'package:test_mge_achmad_irfanda/resource/apps_publicmodel.dart';

class AppsHomePage extends StatefulWidget {
  const AppsHomePage({super.key});

  @override
  State<AppsHomePage> createState() => _AppsHomePageState();
}

class _AppsHomePageState extends State<AppsHomePage> {
  @override
  Widget build(BuildContext context) {
    const jarak = SizedBox(height: 20);
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: const AppAppbar(
        title: "Test Apps - Irfanda",
      ),
      body: Align(
        alignment: Alignment.topCenter,
        child: Container(
          width: 250,
          margin: const EdgeInsets.only(top: 30, ),
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(bottom: 100),
            child: Column(
              children: [
                AppButton(
                    buttonType: ButtonType.bigButton,
                    titleButton: "Input Box",
                    navigatorButton: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const AppsInputBoxPage()));
                    }),
                jarak,
                AppButton(
                    buttonType: ButtonType.bigButton,
                    titleButton: "List View",
                    navigatorButton: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => AppsListViewPage(
                            (item) {
                                  AppsPublicModel.listViewsModel
                                      .add(item);
                                  setState(() {});
                                },


                          )));
                    }),
                jarak,
                AppButton(
                    buttonType: ButtonType.bigButton,
                    titleButton: "Image",
                    navigatorButton: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const AppsImagePage()));
                    }),
                jarak,
                AppButton(
                    buttonType: ButtonType.bigButton,
                    titleButton: "Get Data From API",
                    navigatorButton: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const AppsGetDataFromAPIPage()));
                    }),
                jarak,
                AppButton(
                    buttonType: ButtonType.bigButton,
                    titleButton: "Shared Preference",
                    navigatorButton: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const AppsSharedPreferencePage()));
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
