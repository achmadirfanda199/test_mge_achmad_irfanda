// apps get data from api

import 'package:test_mge_achmad_irfanda/common_import_library.dart';
import 'package:http/http.dart' as https;
import 'dart:convert';

class AppsGetDataFromAPIPage extends StatefulWidget {
  const AppsGetDataFromAPIPage({super.key});

  @override
  State<AppsGetDataFromAPIPage> createState() => _AppsGetDataFromAPIPageState();
}

class _AppsGetDataFromAPIPageState extends State<AppsGetDataFromAPIPage> {
  @override
  Widget build(BuildContext context) {
    final subtitle2 = Theme.of(context).textTheme.subtitle2;
    final caption = Theme.of(context).textTheme.caption;

    Future<List<APIModels>> fetchData() async {
      var url = Uri.parse('https://jsonplaceholder.typicode.com/photos');
      final response = await https.get(url);
      print("response json ${response.body}"); 
      if (response.statusCode == 200) {
        List jsonResponse = json.decode(response.body);
        return jsonResponse.map((data) => APIModels.fromJson(data)).toList();
      } else {
        throw Exception('Unexpected error occured!');
      }
    }

    return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: const AppAppbar(withLeading: true, title: "Get Data form API"),
        body: Container(
            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: FutureBuilder<List<APIModels>>(
              future: fetchData(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      itemCount: snapshot.data!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                            onTap: () {},
                            leading: const SizedBox(
                              height: 70,
                              child: Icon(Icons.list_alt_outlined),
                            ),
                            title: Text(snapshot.data![index].title,
                                style: subtitle2,
                                textAlign: TextAlign.justify,
                                overflow: TextOverflow.ellipsis),
                            subtitle: Text("URI: ${snapshot.data![index].url}",
                                style: caption,
                                textAlign: TextAlign.justify,
                                overflow: TextOverflow.ellipsis));
                      });
                } else if (snapshot.hasError) {
                  return Center(child: Text(snapshot.error.toString(), style: subtitle2,));
                }
                // By default show a loading spinner.
                return const Center(child: CircularProgressIndicator());
              },
            )));
  }
}



/// jika data yang akan di fetch berupa list string, berarti
/// langsung aja di map pada saat get di get api nya
