// apps shared prefreence page

import 'package:test_mge_achmad_irfanda/common_import_library.dart';
import 'package:test_mge_achmad_irfanda/function/ticker_preferences.dart';

class AppsSharedPreferencePage extends StatefulWidget {
  const AppsSharedPreferencePage({super.key});

  @override
  State<AppsSharedPreferencePage> createState() =>
      _AppsSharedPreferencePageState();
}

class _AppsSharedPreferencePageState extends State<AppsSharedPreferencePage> {
  @override
  Widget build(BuildContext context) {
    final subtitle2 = Theme.of(context).textTheme.subtitle2;

    // increment
    void increment() {
      setState(() {
        AppsPublicVariable.valueIncrement =
            AppsPublicVariable.valueIncrement + 1;
      });
    }

    // shared preferences

    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: const AppAppbar(
        title: "SharedPreferences Increment",
        withLeading: true,
      ),
      floatingActionButton: Consumer<TickerPreferences>(
        builder: (context, notifier, child) => FloatingActionButton(
            child: Icon(
              Icons.add,
              color: Theme.of(context).scaffoldBackgroundColor,
            ),
            onPressed: () {
              notifier.toogleChangeValue();
              //increment
              increment();
            }),
      ),
      body: Center(child: Consumer<TickerPreferences>(
        builder: (context, TickerPreferences notifier, child) {
          return Text(
            (notifier.value == AppsPublicVariable.valueIncrement) ? notifier.value.toString() : AppsPublicVariable.valueIncrement.toString(), 
            style: subtitle2!.copyWith(fontSize: 24),
          );
        },
      )),
    );
  }
}
