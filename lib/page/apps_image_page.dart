// apps image page
import 'package:test_mge_achmad_irfanda/common_import_library.dart';

class AppsImagePage extends StatefulWidget {
  const AppsImagePage({super.key});

  @override
  State<AppsImagePage> createState() => _AppsImagePageState();
}

class _AppsImagePageState extends State<AppsImagePage> {
  // function get data image
  Future getImageGallery() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    File file = File(image!.path);
    setState(() {
      AppsPublicVariable.gambarFile = file;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          getImageGallery();
        },
        child: Icon(
          Icons.add,
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
      ),
      appBar: const AppAppbar(
        withLeading: true,
        title: "Image",
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: (AppsPublicVariable.gambarFile == null)
            ? const SizedBox()
            : Center(
                child: Image.file(AppsPublicVariable.gambarFile!,
                    fit: BoxFit.fill)),
      ),
    );
  }
}
