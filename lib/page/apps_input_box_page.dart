// input box apps

// ignore_for_file: deprecated_member_use

import 'package:intl/intl.dart';
import 'package:test_mge_achmad_irfanda/common_import_library.dart';

class AppsInputBoxPage extends StatefulWidget {
  const AppsInputBoxPage({super.key});

  @override
  State<AppsInputBoxPage> createState() => _AppsInputBoxPageState();
}

class _AppsInputBoxPageState extends State<AppsInputBoxPage> {
  @override
  Widget build(BuildContext context) {
    final bodytext1 = Theme.of(context).textTheme.bodyText1;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: const AppAppbar(
        withLeading: true,
        title: "Input Box",
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(bottom: 200),
        child: Container(
            margin:
                const EdgeInsets.only(left: 16, right: 16, top: 24, bottom: 16),
            child: Form(
              key: AppsPublicVariable.formkey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // row textfield 1 and button 1
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 1.8,
                        child: AppsTextFormField(
                          controller: AppsPublicVariable.ubahTextController,
                          label: "Input Text",
                          hintText: "Ayam Bakar Nusantara",
                          namafield: "Input Text",
                          maxLines: 1,
                          keyboardType: TextInputType.text,
                          onChanged: (value) {},
                          onsaved: (value) {
                            AppsPublicVariable.textAsli = value;
                          },
                        ),
                      ),
                      AppsPublicVariable.jarakUniversal,
                      Expanded(
                        child: AppButton(
                            buttonType: ButtonType.normalButton,
                            titleButton: "Run Text",
                            navigatorButton: () {
                              ubahTextFunction(context, setState);
                            }),
                      )
                    ],
                  ),
                  AppsPublicVariable.jarakUniversal,
                  Text(
                    "Text asli: ${AppsPublicVariable.textAsli}",
                    style: bodytext1,
                  ),
                  Text(
                    "A Jadi O: ${AppsPublicVariable.ubahAjadiO}",
                    style: bodytext1,
                  ),
                  Text(
                    "Huruf kecil: ${AppsPublicVariable.hurufKecil}",
                    style: bodytext1,
                  ),
                  Text(
                    "Jumlah huruf: ${AppsPublicVariable.jumalhHuruf}",
                    style: bodytext1,
                  ),
      
                  AppsPublicVariable.jarakAgakJauh,
                  // row textfield 2 and button 2
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 1.8,
                        child: AppsTextFormField(
                          controller: AppsPublicVariable.ubahAngkaController,
                          label: "Input Angka",
                          hintText: "1234567890",
                          namafield: "InputAngka",
                          maxLines: 1,
                          keyboardType: TextInputType.number,
                          onChanged: (value) {},
                          onsaved: (value) {
                            AppsPublicVariable.angkaAsli = value;
                          },
                        ),
                      ),
                      AppsPublicVariable.jarakUniversal,
                      Expanded(
                        child: AppButton(
                            buttonType: ButtonType.normalButton,
                            titleButton: "Run Angka",
                            navigatorButton: () {
                              ubahAngkaFunction(context, setState);
                            }),
                      )
                    ],
                  ),
                  AppsPublicVariable.jarakUniversal,
                  Text(
                    "Angka asli: ${AppsPublicVariable.angkaAsli}",
                    style: bodytext1,
                  ),
                  Text(
                    "Format: ${NumberFormat.currency(
                      symbol: '',
                      decimalDigits: 0,
                      locale: 'id',
                    ).format(AppsPublicVariable.angkaFormat).replaceAll(".", ",")}",
                    style: bodytext1,
                  ),
                  Text(
                    "Akar: ${AppsPublicVariable.akarPangkat}",
                    style: bodytext1,
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
