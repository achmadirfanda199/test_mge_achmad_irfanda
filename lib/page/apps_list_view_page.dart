// apps list view page

import 'package:test_mge_achmad_irfanda/common_import_library.dart';
import 'package:test_mge_achmad_irfanda/function/list_view_function.dart';
import 'package:test_mge_achmad_irfanda/resource/apps_publicmodel.dart';
import 'package:test_mge_achmad_irfanda/widget/apps_list_view.dart';

typedef OnVehicleAdded = void Function(ListViewModel);

class AppsListViewPage extends StatefulWidget {
  final OnVehicleAdded onVehicleAdded;
  const AppsListViewPage(this.onVehicleAdded, {super.key});

  @override
  State<AppsListViewPage> createState() => _AppsListViewPageState();
}

class _AppsListViewPageState extends State<AppsListViewPage> {
  @override
  Widget build(BuildContext context) {
    final ScrollController scrollController = ScrollController();
    final subtitle2 = Theme.of(context).textTheme.subtitle2;
    final caption = Theme.of(context).textTheme.caption;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: const AppAppbar(
        withLeading: true,
        title: "ListView",
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(top: 24, left: 16, right: 16),
            child: Form(
              key: AppsPublicVariable.formkey,
              child: Column(
                children: [
                  //form list view
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 3,
                        child: AppsTextFormField(
                          controller:
                              AppsPublicVariable.listViewNamaFormController,
                          label: "Nama",
                          hintText: "Mobil",
                          namafield: "Nama",
                          maxLines: 1,
                          keyboardType: TextInputType.text,
                          onChanged: (value) {},
                          onsaved: (value) {
                            AppsPublicVariable.nama = value;
                          },
                        ),
                      ),
                      AppsPublicVariable.jarakUniversal,
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 4,
                        child: AppsTextFormField(
                          controller: AppsPublicVariable
                              .listViewKapastitasFormController,
                          label: "Kapasitas Mesin",
                          hintText: "3000",
                          namafield: "Kapasitas Mesin",
                          maxLines: 1,
                          keyboardType: TextInputType.number,
                          onChanged: (value) {},
                          onsaved: (value) {
                            AppsPublicVariable.kapasitasMesin =
                                int.parse(value);
                          },
                        ),
                      ),
                      AppsPublicVariable.jarakUniversal,
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 4,
                        child: AppsTextFormField(
                          controller:
                              AppsPublicVariable.listViewRodaFormController,
                          label: "Roda",
                          hintText: "4",
                          namafield: "Roda",
                          maxLines: 1,
                          keyboardType: TextInputType.number,
                          onChanged: (value) {},
                          onsaved: (value) {
                            AppsPublicVariable.roda = int.parse(value);
                          },
                        ),
                      ),
                    ],
                  ),
                  AppsPublicVariable.jarakAgakJauh,
                  AppButton(
                      buttonType: ButtonType.normalButton,
                      titleButton: "Add",
                      navigatorButton: () {
                        for (var element in AppsPublicModel.listViewsModel) {
                          if (AppsPublicVariable.nama == element.nama) {}
                        }
                        listViewFunction(context, setState, widget);
                      }),

                  AppsPublicVariable.jarakAgakJauh,
                  const Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: AppsListView(
                scrollController: scrollController,
                listData: AppsPublicModel.listViewsModel,
                itemBuilder: (context, index) {
                  var vehicle = AppsPublicModel.listViewsModel[index];
                  int panjangColorModel = AppsPublicModel.colorsTitle.length;
                  int urutanColor = 0;

                  if (index < panjangColorModel) {
                    urutanColor = index;
                  }

                  if (index > panjangColorModel) {
                    urutanColor = 2;
                  }
                  return ListTile(
                      tileColor: AppsPublicModel.colorsTitle[urutanColor],
                      onTap: () {},
                      leading: const SizedBox(
                        height: 70,
                        child: Icon(Icons.list_alt_outlined),
                      ),
                      title: Text(vehicle.nama,
                          style: subtitle2,
                          textAlign: TextAlign.justify,
                          overflow: TextOverflow.ellipsis),
                      subtitle: Text(
                          "Kapasitas Mesin: ${vehicle.kapasitasMesin} / Roda: ${vehicle.roda}",
                          style: caption,
                          textAlign: TextAlign.justify,
                          overflow: TextOverflow.ellipsis));
                }),
          )
        ],
      ),
    );
  }
}
