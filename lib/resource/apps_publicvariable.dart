// apps public variable

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

class AppsPublicVariable {
  static String errorMessage = "";
  static Widget jarakUniversal = const SizedBox(width: 12);
  static Widget jarakAgakJauh = const SizedBox(height: 20);
  static GlobalKey<FormState> formkey = GlobalKey<FormState>();

// public variable ubah text
  static TextEditingController ubahTextController = TextEditingController();
  static String textAsli = "";
  static String ubahAjadiO = "";
  static String hurufKecil = "";
  static int jumalhHuruf = 0;

  // public variable ubah angka
  static TextEditingController ubahAngkaController = TextEditingController();
  static String angkaAsli = "";
  static int angkaFormat = 0;
  static double akarPangkat = 0;

  // public bariable list veiw
  static TextEditingController listViewNamaFormController =
      TextEditingController();
  static TextEditingController listViewKapastitasFormController =
      TextEditingController();
  static TextEditingController listViewRodaFormController =
      TextEditingController();
  static String nama = "";
  static int kapasitasMesin = 0;
  static int roda = 0;

  //image
  static File? gambarFile;

  // value increment shared prefereences
  static int valueIncrement = 0; 
}
