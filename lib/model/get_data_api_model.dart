// api models

import 'package:meta/meta.dart';
import 'dart:convert';

List<APIModels> welcomeFromJson(String str) =>
    List<APIModels>.from(json.decode(str).map((x) => APIModels.fromJson(x)));

String welcomeToJson(List<APIModels> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class APIModels {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  APIModels({
    required this.albumId,
    required this.id,
    required this.title,
    required this.url,
    required this.thumbnailUrl,
  });

  factory APIModels.fromJson(Map<String, dynamic> json) => APIModels(
        albumId: json["albumId"],
        id: json["id"],
        title: json["title"],
        url: json["url"],
        thumbnailUrl: json["thumbnailUrl"],
      );

  Map<String, dynamic> toJson() => {
        "albumId": albumId,
        "id": id,
        "title": title,
        "url": url,
        "thumbnailUrl": thumbnailUrl,
      };
}

// //-> get data kategori
// Future<KategoriModel> get() async {
//   final response =
//       await client.get(Uri.http("10.0.2.2:8000", "api/v2/kategori"),
//           // Uri.https("achmadirfanda.my.id", "api/v2/kategori"),
//           headers: {
//         HttpHeaders.acceptHeader: "application/json",
//         HttpHeaders.contentTypeHeader: "application/json",
//         HttpHeaders.authorizationHeader: "Bearer $accessToken"
//       });
//   print("response get data kategori: ${response.body}");
//   if (response.statusCode == 200) {
//     final responseJSON = json.decode(response.body);
//     return KategoriModel.fromMap(responseJSON);
//   } else {
//     throw ("Invalid token, silakan login ulang!.");
//   }
// }
