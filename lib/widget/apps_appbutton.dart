// apps button

// ignore_for_file: deprecated_member_use

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

enum ButtonType { bigButton, normalButton, onlyTextButton }

class AppButton extends StatelessWidget {
  final ButtonType buttonType;
  final String titleButton;
  final Function() navigatorButton;

  const AppButton({
    super.key,
    required this.buttonType,
    required this.titleButton,
    required this.navigatorButton,
  });

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final buttonColor = Theme.of(context).primaryColorLight;

    final styeleDialogButton = ElevatedButton.styleFrom(
      minimumSize: const Size.fromHeight(20),
      backgroundColor: buttonColor,
    );

    final styleNormalButton = ElevatedButton.styleFrom(
        minimumSize: const Size.fromHeight(50),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
          side: BorderSide(color: primaryColor, width: 1),
        ),
        elevation: 0);

    final styleBigButton = ElevatedButton.styleFrom(
        minimumSize: const Size.fromHeight(70),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(color: primaryColor, width: 1),
        ),
        elevation: 0);

    final textthemeButton = Theme.of(context).textTheme.button;

    return (buttonType == ButtonType.onlyTextButton)
        ? ElevatedButton(
            style: styeleDialogButton,
            onPressed: navigatorButton,
            child: Text(
              titleButton,
              style:
                  textthemeButton!.copyWith(fontSize: 12, color: primaryColor),
            ))
        : (buttonType == ButtonType.normalButton)
            ? ElevatedButton(
                style: styleNormalButton,
                onPressed: navigatorButton,
                child: Text(
                  titleButton,
                  style: textthemeButton!
                      .copyWith(fontSize: 16, color: primaryColor),
                ))
            : ElevatedButton(
                style: styleBigButton,
                onPressed: navigatorButton,
                child: Text(
                  titleButton,
                  style: textthemeButton!.copyWith(
                      fontSize: 16,
                      color: primaryColor,
                      fontWeight: FontWeight.w500),
                ));
  }
}
