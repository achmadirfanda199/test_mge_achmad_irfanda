// apps list view

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

class AppsListView extends StatelessWidget {
final ScrollController scrollController;
  final List<dynamic> listData;
  bool withdivider;
  final Widget Function(BuildContext, int) itemBuilder;

  AppsListView({
    Key? key,
    required this.scrollController,
    this.withdivider = true,
    required this.listData,
    required this.itemBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(bottom: 200),
      controller: scrollController,
      physics: const AlwaysScrollableScrollPhysics(),
      itemCount: listData.length,
      separatorBuilder: (_, index) {
        return const SizedBox();
      },
      itemBuilder: itemBuilder,
    );
  }
}
