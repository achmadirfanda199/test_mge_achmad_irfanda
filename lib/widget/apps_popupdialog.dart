// app pop up dialog


import 'package:test_mge_achmad_irfanda/common_import_library.dart';



enum TypeDialog {
  detail,
  error,
}

enum NavType {
  pop,
}

void appsPopUpDialog(
  BuildContext context,
  TypeDialog typeDialog,
  NavType navigatorType,
  oneLineMessage,
  titleButton,
) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      final subtitle2 = Theme.of(context).textTheme.subtitle2;
      final scaffoldBackgroundColor = Theme.of(context).scaffoldBackgroundColor;
    
      //-> jarak antar content
      const jarak16 = SizedBox(height: 16);
      const jarak6 = SizedBox(height: 6);

      dynamic icon;
      dynamic title;


      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        elevation: 5,
        backgroundColor: scaffoldBackgroundColor,
        scrollable: true,
        contentPadding: const EdgeInsets.all(4),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.center,
              child: icon,
            ),
            
            if (oneLineMessage != null)
              Container(
                  width: 280,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  child: Text(
                    oneLineMessage,
                    style: subtitle2,
                    textAlign: TextAlign.center,
                  )),
            jarak6,
            Text(
              "Terimakasih.",
              style: subtitle2,
              textAlign: TextAlign.center,
            ),
            jarak16,
            
          ],
        ),
      );
    },
  );
}
