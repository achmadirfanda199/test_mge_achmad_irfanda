// custom appbar

// ignore_for_file: deprecated_member_use

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

class AppAppbar extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final bool? withLeading;
  final bool? customNav;
  final dynamic onPressedCustomNav;
  final dynamic withAction;
  final dynamic listAction;

  const AppAppbar({
    super.key,
    required this.title,
    this.withLeading = false,
    this.customNav = false,
    this.onPressedCustomNav,
    this.withAction,
    this.listAction,
  });

  @override
  State<AppAppbar> createState() => _AppAppbarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _AppAppbarState extends State<AppAppbar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: (widget.withLeading == false)
          ? null
          : AppIcon(
              iconType: IconType.materialIconButton,
              colorIcon: Theme.of(context).scaffoldBackgroundColor,
              uriIcon: Icons.arrow_back_rounded,
              uriIconButton: () {
                widget.customNav == true
                    ? widget.onPressedCustomNav
                    : Navigator.of(context).pop();
              },
            ),
      title: Text(
        widget.title,
        style: Theme.of(context).textTheme.headline6!.copyWith(
              fontWeight: FontWeight.w600,
              color: Colors.white,
            ),
      ),
      actions: (widget.withAction == false) ? null : widget.listAction,
      automaticallyImplyLeading: false,
      centerTitle: true,
      backgroundColor: Theme.of(context).primaryColor,
    );
  }
}
