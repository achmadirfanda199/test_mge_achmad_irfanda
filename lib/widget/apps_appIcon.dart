// app icon

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

enum IconType { imageIconButton, materialIconButton, materialIcon }

class AppIcon extends StatelessWidget {
  final dynamic colorIcon;
  final IconType iconType;
  final dynamic uriIcon;
  final dynamic uriIconButton;
  const AppIcon({
    super.key,
    this.colorIcon,
    required this.iconType,
    this.uriIcon,
    this.uriIconButton,
  });

  @override
  Widget build(BuildContext context) {
    final sizeIcon = Theme.of(context).primaryIconTheme.size;

    return (iconType == IconType.imageIconButton)
        ? IconButton(
            onPressed: uriIconButton,
            icon: ImageIcon(
              uriIcon,
              size: sizeIcon,
            ))
        : (iconType == IconType.materialIconButton)
            ? IconButton(
                onPressed: uriIconButton,
                icon: Icon(
                  uriIcon,
                  size: sizeIcon,
                  color: colorIcon,
                ))
            : Icon(
                uriIcon,
                size: sizeIcon,
                color: colorIcon,
              );
  }
}
