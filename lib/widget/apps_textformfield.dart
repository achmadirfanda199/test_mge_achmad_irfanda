// apps textfield
// ignore_for_file: deprecated_member_use, unrelated_type_equality_checks

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

class AppsTextFormField extends StatefulWidget {
  final TextEditingController controller;
  final String label;
  final String hintText;
  final String namafield;
  final int maxLines;
  final TextInputType keyboardType;
  final Function(dynamic) onChanged;
  final Function(dynamic) onsaved;

  const AppsTextFormField({
    super.key,
    required this.controller,
    required this.label,
    required this.hintText,
    required this.namafield,
    required this.maxLines,
    required this.keyboardType,
    required this.onChanged,
    required this.onsaved,
  });

  @override
  State<AppsTextFormField> createState() => _AppsTextFormFieldState();
}

class _AppsTextFormFieldState extends State<AppsTextFormField> {
  @override
  Widget build(BuildContext context) {
    final bodyText1 = Theme.of(context).textTheme.bodyText1;
    final hoverColor = Theme.of(context).primaryColor;
    const disabledColor = Colors.black;
    return TextFormField(
      controller: widget.controller,
      style: bodyText1,
      enabled: true,
      maxLines: widget.maxLines,
      textAlign: TextAlign.justify,
      keyboardType: widget.keyboardType,
      decoration: InputDecoration(
        hoverColor: hoverColor,
        labelText: widget.label,
        labelStyle: bodyText1,
        hintText: widget.hintText,
        hintStyle: bodyText1!
            .copyWith(color: const Color.fromARGB(255, 186, 186, 186)),
        contentPadding: const EdgeInsets.only(left: 20, top: 12, bottom: 12),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: hoverColor)),
        disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: const BorderSide(color: disabledColor)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: hoverColor)),
      ),
      onChanged: (value) {
        widget.onChanged(value);
      },
      onSaved: (value) {
        widget.onsaved(value);
      },
    );
  }
}
