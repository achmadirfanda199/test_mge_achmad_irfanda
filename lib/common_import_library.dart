// file data import librari apblikasi

export 'package:flutter/material.dart';
export 'package:provider/provider.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:google_fonts/google_fonts.dart';
export 'package:image_picker/image_picker.dart';
export 'package:dio/dio.dart';
export 'dart:io';
export 'package:easy_splash_screen/easy_splash_screen.dart';
export 'package:test_mge_achmad_irfanda/widget/apps_button_home_page.dart';
export 'package:test_mge_achmad_irfanda/widget/apps_appIcon.dart';
export 'package:test_mge_achmad_irfanda/resource/apps_publicvariable.dart';
export 'package:test_mge_achmad_irfanda/style/apps_style.dart';
export 'package:test_mge_achmad_irfanda/model/get_data_api_model.dart';
export 'package:test_mge_achmad_irfanda/model/list_view_moodel.dart';
export 'package:test_mge_achmad_irfanda/widget/apps_appbar.dart';
export 'package:test_mge_achmad_irfanda/page/apps_input_box_page.dart';
export 'package:test_mge_achmad_irfanda/page/apps_list_view_page.dart';
export 'package:test_mge_achmad_irfanda/page/apps_image_page.dart';
export 'package:test_mge_achmad_irfanda/page/apps_get_data_from_api_page.dart';
export 'package:test_mge_achmad_irfanda/page/apps_shared_preference_page.dart';
export 'package:test_mge_achmad_irfanda/widget/apps_textformfield.dart';
export 'package:test_mge_achmad_irfanda/widget/apps_appbutton.dart';
export 'package:test_mge_achmad_irfanda/function/ubah_text_function.dart';
export 'package:test_mge_achmad_irfanda/function/ubah_angka_function.dart';
