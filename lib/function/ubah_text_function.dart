// function run text

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

void ubahTextFunction(context, setState) {
  if (AppsPublicVariable.formkey.currentState!.validate()) {
    AppsPublicVariable.formkey.currentState!.save();

    const find1 = "A";
    const find2 = "a";
    const replaceWith1 = "O";
    const replaceWith2 = "o";

    var hasilSementara = "";

    setState(() {
      hasilSementara =
          AppsPublicVariable.textAsli.replaceAll(find1, replaceWith1);
      AppsPublicVariable.ubahAjadiO =
          hasilSementara.replaceAll(find2, replaceWith2);
      AppsPublicVariable.hurufKecil = AppsPublicVariable.textAsli.toLowerCase();
      AppsPublicVariable.jumalhHuruf = AppsPublicVariable.textAsli.length;
    });
  }
}
