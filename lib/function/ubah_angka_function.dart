// ubah angka function

import 'dart:math';

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

void ubahAngkaFunction(context, setState) {
  if (AppsPublicVariable.formkey.currentState!.validate()) {
    AppsPublicVariable.formkey.currentState!.save();
    setState(() {
      AppsPublicVariable.angkaFormat = int.parse(AppsPublicVariable.angkaAsli); 
      AppsPublicVariable.akarPangkat =
          sqrt(int.parse(AppsPublicVariable.angkaAsli));
    });
  }
}
