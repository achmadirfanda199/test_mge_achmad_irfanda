// sahred preferennces to save increment values

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

class TickerPreferences extends ChangeNotifier {
  String key = "valueIncrement";
  late SharedPreferences _preferences;
  late int value; 
  TickerPreferences() {
    value = AppsPublicVariable.valueIncrement;
    _loadFromPreferences();
  }

  _initialPreferences() async {
    _preferences = await SharedPreferences.getInstance();
  }

  _savePreferences() async {
    await _initialPreferences();
    _preferences.setInt(key, AppsPublicVariable.valueIncrement);
  }

  _loadFromPreferences() async {
    return _initialPreferences();
  }

  toogleChangeValue() {
    AppsPublicVariable.valueIncrement;
    _savePreferences();
    notifyListeners();
  }
}
