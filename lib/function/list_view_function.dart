// list view function

import 'package:test_mge_achmad_irfanda/common_import_library.dart';
import 'package:test_mge_achmad_irfanda/model/list_view_moodel.dart';
import 'package:test_mge_achmad_irfanda/resource/apps_publicmodel.dart';

void listViewFunction(context, setState, widget) async {
  if (AppsPublicVariable.formkey.currentState!.validate()) {
    AppsPublicVariable.formkey.currentState!.save();

    String nama = "";
    int kepasitasMesin = 0;
    int roda = 0;

    print("ini nama ${AppsPublicVariable.nama}");

    final kendaraan = AppsPublicModel.listViewsModel.firstWhere(
        (s) => s.nama == AppsPublicVariable.nama,
        orElse: () => ListViewModel(
            nama: AppsPublicVariable.nama,
            kapasitasMesin: AppsPublicVariable.kapasitasMesin,
            roda: AppsPublicVariable.roda));

    if (kendaraan.nama == AppsPublicVariable.nama) {
      setState(() {
        nama = kendaraan.nama;
        kepasitasMesin = kendaraan.kapasitasMesin;
        roda = kendaraan.roda;
      });
    }
    widget.onVehicleAdded(ListViewModel(
        nama: AppsPublicVariable.nama = nama,
        kapasitasMesin: AppsPublicVariable.kapasitasMesin = kepasitasMesin,
        roda: AppsPublicVariable.roda = roda)); // Navigator.of(context).pop();

    print(AppsPublicModel.listViewsModel.length);

    //clear value in textfield
    AppsPublicVariable.listViewNamaFormController.clear();
    AppsPublicVariable.listViewKapastitasFormController.clear();
    AppsPublicVariable.listViewRodaFormController.clear();
  }
}
