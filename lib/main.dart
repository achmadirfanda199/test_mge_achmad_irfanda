// main apps

import 'package:test_mge_achmad_irfanda/function/ticker_preferences.dart';
import 'package:test_mge_achmad_irfanda/page/apps_splash_scree.dart';

import 'common_import_library.dart';

void main() {
  runApp(const MyApps());
}

class MyApps extends StatefulWidget {
  const MyApps({super.key});

  @override
  State<MyApps> createState() => _MyAppsState();
}

class _MyAppsState extends State<MyApps> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TickerPreferences>(create: (_) => TickerPreferences()),
      ],
      child: GestureDetector(
          onTap: () {
            FocusScopeNode curentFocus = FocusScope.of(context);
            if (!curentFocus.hasPrimaryFocus) {
              curentFocus.unfocus();
            }
          },
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            routes: {
              "/": (_) => const SplashScreen()
            },
          )),
    );
  }
}
