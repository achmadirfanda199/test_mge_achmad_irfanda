// app style

// ignore_for_file: deprecated_member_use

import 'package:test_mge_achmad_irfanda/common_import_library.dart';

class AppStyles {
  final backgroundDark = Color(int.parse("FF9DC08B", radix: 16));
  final backgroundLigt = Color(int.parse("ffedf1d6", radix: 16));
  final primaryColor = Colors.blue; 
  final secondaryColor = Color(int.parse("ff9dc0bb", radix: 16));
  final textLight = Colors.white;
  final textDark = Colors.black;

  ThemeData themeDataApps(BuildContext context) {
    return ThemeData(
      // main user color apps
      primaryColor: primaryColor,
      scaffoldBackgroundColor: backgroundLigt,
      // main user color textApps
      primaryColorLight: textLight,
      primaryColorDark: textDark,
      // main user button color apps

        textTheme: TextTheme(
      headline1: GoogleFonts.poppins(
          fontSize: 93, fontWeight: FontWeight.w300, letterSpacing: -1.5),
      headline2: GoogleFonts.poppins(
          fontSize: 58, fontWeight: FontWeight.w300, letterSpacing: -0.5),
      headline3: GoogleFonts.poppins(fontSize: 47, fontWeight: FontWeight.w400),
      headline4: GoogleFonts.poppins(
          fontSize: 33, fontWeight: FontWeight.w400, letterSpacing: 0.25),
      headline5: GoogleFonts.poppins(fontSize: 23, fontWeight: FontWeight.w400),
      headline6: GoogleFonts.poppins(
          fontSize: 19, fontWeight: FontWeight.w500, letterSpacing: 0.15),
      subtitle1: GoogleFonts.poppins(
          fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.15),
      subtitle2: GoogleFonts.poppins(
          fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1),
      bodyText1: GoogleFonts.poppins(
          fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.5),
      bodyText2: GoogleFonts.poppins(
          fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.25),
      button: GoogleFonts.poppins(
          fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 1.25),
      caption: GoogleFonts.poppins(
          fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
      overline: GoogleFonts.poppins(
          fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5),
    ));
  }
}
